import csv
from mysql.connector import connect

path_ca = "category.csv"
path_fo = "food_item.csv"
file_ca = open(path_ca, newline="")
file_fo = open(path_fo, newline="")
reader_ca = csv.reader(file_ca)
reader_fo = csv.reader(file_fo)

header_ca = next(reader_ca)
header_fo = next(reader_fo)

# print(header_ca)
# print(header_fo)

data_ca = []
data_fo = []
for row in reader_ca:
    data_ca.append(row[0])

for row in reader_fo:
    # row = [nameFood, category, priceFood, amountFood, imageUrlFood]
    category = int(row[1])
    priceFood = float(row[2])
    amountFood = 1

    data_fo.append((row[0], category, priceFood, amountFood, row[4]))

# print(data_fo)
# print(data_ca)
db = connect(
    host="localhost",
    user="root",
    passwd="pallab-dev",
    database="oros",
    auth_plugin="mysql_native_password",
)

cur = db.cursor()
# for var in data_ca:
#    sql = 'INSERT INTO category (nameCategory) VALUES ("' + var + '")'
#    print(sql)
#    cur.execute(sql)
#    db.commit()
for var in data_fo:
    sql1 = "INSERT INTO food (nameFood, category, priceFood,"
    sql1 += " amountFood, imageUrlFood) VALUES " + str(var)
    print(sql1)
    cur.execute(sql1)
    db.commit()
