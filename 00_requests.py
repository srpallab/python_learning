#!/usr/bin/python3
""" This is only lerning file for request module
"""

import requests


def main():
    # response = requests.get("http://www.google.com")
    # payload = {"base": "USD", "symbols": "BDT"}
    payload = {"base": "USD", "symbols": "JPY"}
    response = requests.get(
        "https://api.exchangeratesapi.io/latest",
        params=payload
    )
    if response.status_code != 200:
        print("Status Code: ", response.status_code)
        raise Exception("There is an error!")
    data = response.json()
    print("JSON Data: ", data)


if __name__ == '__main__':
    main()
