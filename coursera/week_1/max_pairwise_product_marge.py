def qs_s(num):
    less = []
    equal = []
    grater = []
    if len(num) > 1:
        pivot = num[0]
        for i in num:
            if i < pivot:
                less.append(i)
            elif i > pivot:
                grater.append(i)
            else:
                equal.append(i)
        return qs_s(less) + equal + qs_s(grater)
    else:
        return num


if __name__ == "__main__":
    input_n = int(input())
    input_numbers = [int(x) for x in input().split()]
    len_num = len(input_numbers)
    in_s = qs_s(input_numbers)
    max_product = in_s[len_num - 1] * in_s[len_num - 2]
    print(max_product)
