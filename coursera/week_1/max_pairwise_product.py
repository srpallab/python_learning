if __name__ == "__main__":
    input_n = int(input())
    input_numbers = [int(x) for x in input().split()]
    input_numbers.sort()
    len_num = len(input_numbers)
    max_product = input_numbers[len_num - 1] * input_numbers[len_num - 2]
    print(max_product)
