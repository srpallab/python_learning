from time import time

# from timeit import timeit
from random import randint  # , random

# One Line marge sort style


def q_st(num):
    if len(num) < 2:
        return num
    return (
        q_st([i for i in num if i < num[0]])
        + [num[0]]
        + q_st([i for i in num if i > num[0]])
    )


# Stack Overflow best answer


def qs_s(num):
    less = []
    equal = []
    grater = []
    if len(num) > 1:
        pivot = num[0]
        for i in num:
            if i < pivot:
                less.append(i)
            elif i > pivot:
                grater.append(i)
            else:
                equal.append(i)
        return qs_s(less) + equal + qs_s(grater)
    else:
        return num


# Normal C style In place technique


def partitioning(num, start, end):
    p_index = start
    pivot = num[end]
    for i in range(start, end):
        if num[i] < pivot:
            num[p_index], num[i] = num[i], num[p_index]
            p_index += 1
    num[end], num[p_index] = num[p_index], num[end]
    return p_index


def quick_sort(num, start, end):
    if start < end:
        p_index = partitioning(num, start, end)
        quick_sort(num, start, p_index - 1)
        quick_sort(num, p_index + 1, end)


# Cousera Video


def max_pair(num):
    len_num = len(num)
    max_1 = 0
    for i in range(len_num):
        if num[i] >= num[max_1]:
            max_1 = i
    max_2 = 0
    for i in range(len_num):
        if num[i] >= num[max_2] and i != max_1:
            max_2 = i
    # print(num[max_1], num[max_2], num[max_1] * num[max_2])
    return num[max_1] * num[max_2]


if __name__ == "__main__":
    # input_n = int(input())
    # input_numbers = [int(x) for x in input().split()]
    # Creating Large number list
    input_numbers = []
    for i in range(1000):
        input_numbers.append(randint(1, 10000))
    # One line random list
    # Here _ is literal Grammer ex: 1_000 means 1000
    # Just Readability Improved
    # Some other also support it
    # It started from python 3.6.1
    # https://www.python.org/dev/peps/pep-0515/#literal-grammar
    # randomList = [random() for num in range(1_000)]
    # print(randomList)
    ##########################################
    # Max pair python built in sort technique
    ##########################################
    t0 = time()
    input_num_1 = input_numbers
    input_num_1.sort()
    len_num = len(input_num_1)
    max_pro = input_num_1[len_num - 1] * input_num_1[len_num - 2]
    t1 = time()
    print(max_pro, t1 - t0)
    ######################################
    # Max pair quick sort technique
    # In place quick sort
    # but very slow and runs into recursion error with lots of items
    ######################################
    input_num_2 = input_numbers
    t0 = time()
    len_num = len(input_num_2)
    quick_sort(input_num_2, 0, len_num - 1)
    max_pro = input_num_2[len_num - 1] * input_num_2[len_num - 2]
    t1 = time()
    print(max_pro, t1 - t0)
    ##########################
    # Corseria Video technique
    ##########################
    input_num_3 = input_numbers
    t0 = time()
    max_pro = max_pair(input_num_3)
    t1 = time()
    print(max_pro, t1 - t0)

    ############################
    # super simple two-line quick sort
    ############################
    input_num_4 = input_numbers
    t0 = time()
    sor_num = q_st(input_num_4)
    max_pro = input_num_4[len_num - 1] * input_num_4[len_num - 2]
    t1 = time()
    print(max_pro, t1 - t0)
    # time1=timeit("q_st(input_num_4)", globals=globals(), number=100)
    # print(time1)
    ##########################################
    # Simple Quick Sort (like marge technique)
    ##########################################
    input_num_5 = input_numbers
    t0 = time()
    sor_num_2 = qs_s(input_num_5)
    max_pro = input_num_5[len_num - 1] * input_num_5[len_num - 2]
    t1 = time()
    print(max_pro, t1 - t0)
