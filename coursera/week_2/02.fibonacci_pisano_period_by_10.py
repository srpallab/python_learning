def fibonacci(number):
    # number %= 60
    if number < len(fibo_list):
        return fibo_list[number]
    fibo_list.append((fibonacci(number - 1) + fibonacci(number - 2)) % 10)
    return fibo_list[number]


if __name__ == "__main__":
    input_n = int(input())
    number = input_n % 60
    fibo_list = []
    fibo_list.append(0)
    fibo_list.append(1)
    fibonacci(number)
    print(fibo_list[number])
