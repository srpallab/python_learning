def gcd(first, second):
    while second != 0:
        t = second
        second = first % second
        first = t
    return first


if __name__ == "__main__":
    first, second = map(int, input().split())
    print((first * second) // gcd(first, second))
