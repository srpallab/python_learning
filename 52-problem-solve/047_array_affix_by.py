"""
Add Two Array and Sort.
"""
T = int(input())

while T:
    arr1 = [int(i) for i in input().split()]
    arr2 = [int(i) for i in input().split()]
    arr3 = arr1[1 : arr1[0] + 1] + arr2[1 : arr2[0] + 1]
    arr3.sort()
    print(" ".join(str(i) for i in arr3))
    T -= 1
