""" Find the Number is sqare or not
"""
from math import sqrt, floor, ceil

T = int(input())

while T:
    num = int(input())
    sqr_num = sqrt(num)
    if floor(sqr_num) == ceil(sqr_num):
        print("YES")
    else:
        print("NO")
    T -= 1
