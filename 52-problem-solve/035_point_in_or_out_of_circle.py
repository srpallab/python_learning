"""
Finding a point is in or out of a circle.
"""
from math import sqrt

T = int(input())

while T:
    xc, yc = map(float, input().split())
    R = float(input())
    x, y = map(float, input().split())
    line_l = sqrt((xc - x) ** 2 + (yc - y) ** 2)
    # print(xc, yc, R, x, y, line_l)
    if R >= line_l:
        print("The point is inside the circle")
    else:
        print("The point is not inside the circle")
    T -= 1
