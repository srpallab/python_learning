"""
Finding Days to end food if an alien eats
half of his food every day.
"""
T = int(input())

while T:
    num = float(input())
    count = 0
    while num >= 1:
        num /= 2
        count += 1
    print("{} days".format(count))
    T -= 1
