"""
Find Sum of the series.
"""


def factorial(n):
    if n == 1:
        return 1
    return n * factorial(n - 1)


T = int(input())

while T:
    k = int(input())
    sum = 0.0
    for i in range(1, k + 1):
        sum = sum + i / factorial(i)
    print("{0:.4f}".format(sum))
    T -= 1
