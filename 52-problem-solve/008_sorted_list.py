""" Sort a number list.
"""
T = int(input())
TEMP = 1

while T:
    num_list = map(int, input().split())
    sort_num_list = sorted(list(num_list))
    message = "Case {}:".format(TEMP)
    for i in sort_num_list:
        message += " " + str(i)
    print(message)
    TEMP += 1
    T -= 1
