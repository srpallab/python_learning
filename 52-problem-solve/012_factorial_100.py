""" Find How many zero in the end of large factorial number.
"""
T = int(input())

while T:
    num = int(input())
    TEMP = 0
    # print(num)
    while num > 4:
        if num % 5 == 0:
            TEMP += num // 5
            num = num // 5
        else:
            num -= 1
    print(TEMP)
    T -= 1
