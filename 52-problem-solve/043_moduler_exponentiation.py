"""
Find Moduler Expnentiation.
"""


def Mod(p, q, c):
    if q == 0:
        return 1
    elif q % 2 == 0:
        y = Mod(p, q // 2, c)
        return (y * y) % c
    return ((p % c) * Mod(p, q - 1, c)) % c


T = int(input())

while T:
    p, q, c = map(int, input().split())
    print("Result = {}".format(Mod(p, q, c)))
    T -= 1
