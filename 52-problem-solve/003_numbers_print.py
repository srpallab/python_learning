""" Printing Numbers From 1000 to 1.
"""
for i in range(1000, 0, -1):
    print(i, end="\t")
    if (i - 1) % 5 == 0:
        print("")
