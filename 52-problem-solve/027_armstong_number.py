""" Find Armstrong Number.
"""
T = int(input())

while T:
    num = input()
    num_list = [int(i) for i in num]
    # print(num, num_list)
    length = len(num_list)
    sum_all = 0
    for i in num_list:
        sum_all += pow(i, length)
    if sum_all == int(num):
        print("{} is an armstrong number!".format(num))
    else:
        print("{} is not an armstrong number!".format(num))
    T -= 1
