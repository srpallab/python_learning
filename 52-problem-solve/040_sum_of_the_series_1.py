"""
Find Sum of the series.
"""
T = int(input())

while T:
    x, k = map(int, input().split())
    sum = 1
    for i in range(1, k + 1):
        sum = sum + x ** i
    print("Result = " + str(sum))
    T -= 1
