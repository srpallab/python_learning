""" Find How many way to make a sentence.
"""
T = int(input())


def factorial(n):
    if n == 0:
        return 1
    return n * factorial(n - 1)


while T:
    str_list = input().lower().split()
    counts = dict()
    TEMP = 1
    # print(str_list, type(str_list))
    for i in str_list:
        if i in counts:
            counts[i] += 1
        else:
            counts[i] = 1
    # print(counts)
    for i, j in counts.items():
        if j > 1:
            # print(i, j)
            TEMP *= factorial(j)
            # print(TEMP)
    print("1/" + str(factorial(len(str_list)) // TEMP))
    T -= 1
