"""
Display Numbers in a pattern like dimon.
"""
T = int(input())

while T:
    num, b = map(int, input().split())
    # counts = 1
    for i in range(num):
        for j in range(i + 1):
            if j < i:
                print(b, end=" ")
            else:
                print(b)
    for i in range(num - 1, 0, -1):
        for j in range(i, 0, -1):
            if j > 1:
                print(b, end=" ")
            else:
                print(b)

    print()
    T -= 1
