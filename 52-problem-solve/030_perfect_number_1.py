""" Find the Perfect Number.
"""
from math import sqrt

# from time import time


def factor_sum(n):
    sqrt_n = sqrt(n)
    sum_all = 1
    i = 2
    # print("n={},s_n={}".format(n, sqrt_n))
    while i <= sqrt_n:
        if n % i == 0:
            sum_all = sum_all + i + n // i
            # print(sum_all, i)
        i += 1
    return sum_all


T = int(input())

while T:
    num = int(input())
    # t0 = time()
    if factor_sum(num) == num:
        print("YES, {} is a perfect number!".format(num))
    else:
        print("NO, {} is not a perfect number!".format(num))
    # t1 = time()
    # print("Time Required: ", t1 - t0)
    T -= 1
