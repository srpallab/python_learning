"""
Simple Program to find out a number
is odd or even.
"""
# Taking Number input
NUM = int(input())

for n in range(NUM):
    test = input()
    if int(test[-1]) % 2 == 0:
        print("even")
    else:
        print("odd")
