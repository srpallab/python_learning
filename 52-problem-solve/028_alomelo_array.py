""" Finding the series of number inputed is sorted or not.
"""
T = int(input())
TEMP = "NO"
k = []

while T:
    k.append(int(input()))
    T -= 1

for i in range(len(k) - 1):
    if k[i] <= k[i + 1]:
        TEMP = "YES"
    else:
        TEMP = "NO"
        break
print(TEMP)
