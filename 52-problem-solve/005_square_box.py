"""
Simple Square Box Print With Stars
"""
N = int(input())

while N:
    M = int(input())
    for i in range(M):
        for k in range(M):
            print('*', end='')
        if k == M-1 and i == M-1 and N != 1:
            print('\n')
        else:
            print()
    N -= 1
