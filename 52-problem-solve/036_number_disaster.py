""" Number Reverse Print
"""
T = int(input())

while T:
    num = int(input())
    print(int(str(num)[::-1]))
    T -= 1
