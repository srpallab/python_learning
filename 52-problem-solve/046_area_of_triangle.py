""" Find Area Of Triangle.
"""
from math import sqrt

T = int(input())

while T:
    a, b, c = map(int, input().split())
    s = (a + b + c) / 2
    area = sqrt(s * (s - a) * (s - b) * (s - c))
    print("Area = {:.3f}".format(area))
    T -= 1
