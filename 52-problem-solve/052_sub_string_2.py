"""
Find how many times a sub string apperas in a string.
Example: banana
          |||||
          ana||
            |||
            ana
"""
T = int(input())

while T:
    str_o, str_s = input().split()
    # print(str_o, str_s)
    str_t, count = str_o, 0
    for i in range(len(str_o)):
        f_index = str_t.find(str_s)
        if f_index != -1:
            str_t = str_t[f_index + 1 :]
            # print(str_t)
            count += 1
    print(count)
    T -= 1
