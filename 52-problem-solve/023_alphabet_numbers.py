""" Giving Alphabets numbers.
"""
T = int(input())

while T:
    alpabet = input().upper()
    for i in list(alpabet):
        if i.isalpha():
            print(ord(i) - (ord("A") - 1), end="")
    print()
    T -= 1
