""" Find total number of prime number between two numbers.
"""
from math import sqrt, floor
from time import time

SIZE = 100000
arr = [1] * (SIZE + 1)


def sieve():
    arr[0], arr[1] = (0, 0)
    root = floor(sqrt(SIZE))
    # print(root)
    for i in range(2, root + 1):
        if arr[i] == 1:
            j = 2
            while j * i <= SIZE:
                arr[j * i] = 0
                j += 1


T = int(input())

while T:
    start, end = map(int, input().split())
    count = 0
    if start <= 1:
        start = 2

    t0 = time()
    sieve()
    for i in range(start, end + 1):
        if arr[i] == 1:
            count += 1
            # print(count, i, arr[i])
    t1 = time()
    print(count)
    # print("Time Required: ", t1 - t0))
    T -= 1
