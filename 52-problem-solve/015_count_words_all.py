""" Simple Word Count Program.
"""
T = int(input())

while T:
    str_s = input().strip()
    str_ss = sorted(list(set(str_s)))
    # print(str_s, str_ss)
    for i in str_ss:
        print(i + " = " + str(str_s.count(i)))
    if T > 1:
        print()
    T -= 1
