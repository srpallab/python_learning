""" Find the run rate
"""
T = int(input())

while T:
    num = [float(i) for i in input().split()]
    tar_r = num[0] + 1
    ov_b = (300 - num[2]) / 6
    ov_r = num[2] / 6
    c_rr = num[1] / ov_b
    if num[0] < num[1]:
        r_rr = 0.00
    else:
        r_rr = (tar_r - num[1]) / ov_r
    print("{0:.2f} {1:.2f}".format(c_rr, r_rr))
    T -= 1
