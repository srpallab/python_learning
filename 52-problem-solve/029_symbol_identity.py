""" Find the identity of a symbol.
"""
T = int(input())

while T:
    send = input()
    if send.isalpha():
        if send.isupper():
            print("Uppercase Character")
        else:
            print("Lowercase Character")
    elif send.isnumeric():
        print("Numerical Digit")
    else:
        print("Special Character")
    T -= 1
