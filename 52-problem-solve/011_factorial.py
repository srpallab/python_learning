""" Find Factorial of a number with recuirtion.
"""
T = int(input())

while T:
    num = int(input())

    def fact(n):
        if n == 0:
            return 1
        return n * fact(n - 1)

    print(fact(num))
    T -= 1
