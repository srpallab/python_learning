""" Reverse input word.
"""
T = int(input())

while T:
    print(input()[::-1])
    T -= 1
