""" Find LCM
"""
T = int(input())

while T:
    num1, num2 = map(int, input().split())
    a, b = num1, num2
    while b != 0:
        t = b
        b = a % b
        a = t
    # print(a)
    print("LCM = " + str(num1 * num2 // a))
    T -= 1
