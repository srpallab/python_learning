""" Multiplication of X.
"""
T = int(input())

while T:
    x, n = map(int, input().split())
    if x > n:
        print("Invalid!")
    else:
        for k in range(1, int(n / x) + 1):
            print(x * k)
    print()
    T -= 1
