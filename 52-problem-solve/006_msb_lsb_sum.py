"""
A Simple Program that finds MSB and LSB of a number and add them.
"""
TE = int(input())

while TE:
    # For Number thats Lenght is not given
    # N = int(input())
    # LAST_NUMBER = N % 10
    # while N >= 10:
    #     N = N//10
    #     FIRST_NUMBER = N
    # SUM = FIRST_NUMBER + LAST_NUMBER
    # print('Sum = ' + str(SUM))

    # For Number thats Lenght is Fixed
    N = input()
    LAST_NUMBER = int(N[-1])
    FIRST_NUMBER = int(N[0])
    SUM = FIRST_NUMBER + LAST_NUMBER
    print("Sum = " + str(SUM))
    TE -= 1
