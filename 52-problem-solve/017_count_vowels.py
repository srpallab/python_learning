""" Count how many vowels in a sentence.
"""
T = int(input())

while T:
    vowels = ["a", "e", "i", "o", "u"]
    str_a = input().lower().split()
    # print(str_a)
    count = 0
    for i in str_a:
        for j in i:
            # print(j)
            if j in vowels:
                count += 1
    print("Number of vowels = " + str(count))
    T -= 1
