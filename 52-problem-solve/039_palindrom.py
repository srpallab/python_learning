""" Palindrom.
"""
T = int(input())

while T:
    num = input()
    if num == num[::-1]:
        print("Yes! It is Palindrome!")
    else:
        print("Sorry! It is not Palindrome!")
    T -= 1
