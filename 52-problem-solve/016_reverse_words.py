""" Reverce words of a sentence.
"""
T = int(input())

while T:
    print(" ".join([m[::-1] for m in input().split()]))
    T -= 1
