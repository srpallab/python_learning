"""
Find Perfect numbers through 2 to  2^64 - 1
But It takes large amount of time. So the
SIZE variable is low now. Originally run with
large number(2^64-1)
list is [6, 28, 496, 8128, 33550336, 8589869056,
         137438691328, 2305843008139952128]
"""
from math import sqrt

SIZE = 40000
N = 2
fatos = []

while N <= SIZE:
    sqrt_n = sqrt(N)
    sum_all = 1
    i = 2
    # print("n={},s_n={}".format(n, sqrt_n))
    while i <= sqrt_n:
        if N % i == 0:
            sum_all = sum_all + i + N // i
        i += 1
    if N == sum_all:
        fatos.append(N)
    N += 1

data = "Perfect Numbers: " + str(fatos)
with open("perfect_numbers_031.txt", "w") as pf:
    pf.write(data)
