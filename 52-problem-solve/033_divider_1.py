"""
Find all divider of c up to a to b.
"""

T = int(input())

while T:
    a, b, c = map(int, input().split())
    # print(a, b, c)
    while a % c:
        a += 1
    for i in range(a, b + 1, c):
        print(i)
    print()
    T -= 1
