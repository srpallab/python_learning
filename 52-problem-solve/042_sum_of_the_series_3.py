"""
For number N print 2^N to 1.
"""

T = int(input())

while T:
    N = int(input())
    # num_list = [i for i in range(N, 0, -1)]
    for i in range(N, -1, -1):
        if i == 1:
            print("2 + ", end="")
        elif i == 0:
            print("1")
        else:
            print("2^{} + ".format(i), end="")
    T -= 1
