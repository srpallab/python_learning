"""
Find The number is prime or not.
"""
from math import sqrt


def prime(n):
    if n < 2:
        return 0
    elif n == 2:
        return 1
    elif n % 2 == 0:
        return 0
    for i in range(3, int(sqrt(n)) + 1):
        if n % i == 0:
            return 0
    return 1


T = int(input())

while T:
    num = int(input())
    if prime(num) == 1:
        print("{} is a prime".format(num))
    else:
        print("{} is not a prime".format(num))
    T -= 1
