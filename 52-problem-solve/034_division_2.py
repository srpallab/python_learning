"""
Find Common Divider of Two Numbers up to C.
"""


def gcd(a, b):
    while b != 0:
        tem = b
        b = a % b
        a = tem
    return a


def lcm(a, b):
    x, y = a, b
    return x * y // gcd(a, b)


T = int(input())
while T:
    a, b, c = map(int, input().split())
    # print(a, b, c, lcm(a,b))
    hop = lcm(a, b)
    for i in range(hop, c + 1, hop):
        print(i)
    if T != 1:
        print()
    T -= 1
