""" Print Alternate List Elements
"""
T = int(input())

while T:
    num = int(input())
    num_list = [int(i) for i in input().split()]
    temp_list = []
    # print(num, num_list)
    for i in range(0, num, 2):
        temp_list.append(num_list[i])
    print(" ".join(map(str, temp_list)))
    T -= 1
