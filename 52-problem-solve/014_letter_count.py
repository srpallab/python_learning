""" Simple Word Count Program.
"""
T = int(input())

while T:
    str_s = input()
    ltr = str(input())
    count = str_s.count(ltr)
    if count:
        message = "Occurrence of '" + ltr + "' in '" + str_s + "' = "
        print(message + str(count))
    else:
        print("'" + ltr + "' is not present")
    T -= 1
