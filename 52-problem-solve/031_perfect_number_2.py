""" Find All perfet numbers up to 40000000.
"""
T = int(input())
p_list = [6, 28, 496, 8128, 33550336, 8589869056]

while T:
    num = int(input())
    for j in range(6):
        if p_list[j] <= num:
            print(p_list[j])
    if T != 1:
        print()
    T -= 1
