"""
Simple Factor Finder.
"""
from math import sqrt

# Code For Stress Testing
# with open("testcasefile004.txt", 'r') as f:
#    F_CONT = f.readlines()
#    print(F_CONT)

N = int(input())

for k in range(1, N+1):
    n = int(input())
    n_s = int(sqrt(n))
    i = 1
    arr = []
    while i <= n_s:
        if n % i == 0:
            if i != n//i:
                arr.append(i)
                arr.append(n//i)
            else:
                arr.append(i)
            i += 1
        else:
            i += 1
    arr.sort()
    result = 'Case ' + str(k) + ': ' + ' '.join(str(e) for e in arr)
    print(result)
    # Code For Stress Testing
    # if len(result) == len(F_CONT[k-1]):
    #     print(result)
    #     print(F_CONT[k-1])
