"""
Find the sub string first index.
"""
T = int(input())

while T:
    string_o, string_s = input().split()
    index_sub = string_o.find(string_s)
    if index_sub == -1:
        print(0)
    else:
        print(index_sub)
    T -= 1
