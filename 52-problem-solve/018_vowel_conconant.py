""" Display vowels and conconant of a sentence.
"""
T = int(input())

while T:
    vowels = ["A", "E", "I", "O", "U", "a", "e", "i", "o", "u"]
    str_a = input().split()
    # print(str_a)
    str_v = []
    str_c = []
    for i in str_a:
        for j in list(i):
            if j in vowels:
                str_v.append(j)
            elif j.isalpha():
                str_c.append(j)
    print("".join(str_v) + "\n" + "".join(str_c))
    T -= 1
