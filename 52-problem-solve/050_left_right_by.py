"""
Replace number follwing pattern.
"""
T = int(input())

while T:
    alf = input()
    alf_l = [i for i in alf]
    for i in range(len(alf_l)):
        # print(i)
        if alf_l[i] == "L":
            alf_l[i] = alf_l[i - 1]
        elif alf_l[i] == "R":
            alf_l[i] = alf_l[i + 1]
    print("".join(alf_l))
    T -= 1
