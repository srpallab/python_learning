import cv2 as cv
import numpy as np

video_image_dict = {
    "10Million": [
        'media/segment1/left10Million/i0.jpg',
        'media/segment1/right10Million/v0.mp4'
    ],
    "BattleThePower": [
        'media/segment1/leftBattleThePower/i0.jpg',
        'media/segment1/rightBattleThePower/v0.mp4'
    ]
}


def rescaleFrame(frame, scale_width=0.75, scale_height=1.335):
    width = int(frame.shape[1] * scale_width)
    height = int(frame.shape[0] * scale_height)
    dimensions = (width, height)
    return cv.resize(frame, dimensions, interpolation=cv.INTER_AREA)


for key, value in video_image_dict.items():
    image_capture = cv.imread(value[0])
    video_capture = cv.VideoCapture(value[1])
    while (video_capture.isOpened):
        isTrue, frame = video_capture.read()
        if isTrue is True:
            resized_image = rescaleFrame(image_capture, 0.5, 0.3828)
            resized_video_frame = rescaleFrame(frame, 0.75, 0.75)
            concat_frame = np.concatenate(
                (resized_image, resized_video_frame),
                axis=1
            )
            cv.namedWindow('Video')
            cv.moveWindow('Video', 0, 0)
            cv.imshow('Video', concat_frame)
            if cv.waitKey(25) & 0xFF == ord('q'):
                break
        else:
            break
    video_capture.release()
cv.destroyAllWindows()
