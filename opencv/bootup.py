import cv2 as cv
import os

RIGHTWINDOW = 'Right'
LEFTWINDOW = 'Left'
path = r'/home/srpallab/workspace/python_learning/opencv/media/bootup'

right_paths = [
     f.path for f in os.scandir(path) if f.is_dir() and 'right' in f.name
]
left_paths = [
    f.path for f in os.scandir(path) if f.is_dir() and 'left' in f.name
]
# print(right_paths)
# print(left_paths)


def rescaleFrame(frame, scale_width=0.75, scale_height=1.335):
    width = int(frame.shape[1] * scale_width)
    height = int(frame.shape[0] * scale_height)
    dimensions = (width, height)
    return cv.resize(frame, dimensions, interpolation=cv.INTER_AREA)


for right_path in right_paths:
    right_video_file_path = right_path \
        + '/' + os.walk(right_path).__next__()[2][0]
    right_capture = cv.VideoCapture(right_video_file_path)
    while(right_capture.isOpened()):
        rightIsTrue, rightFrame = right_capture.read()
        if rightIsTrue is True:
            cv.namedWindow(RIGHTWINDOW)
            cv.moveWindow(RIGHTWINDOW, 960, 0)
            right_resized_video_frame = rescaleFrame(rightFrame, 0.75, 0.75)
            cv.imshow(RIGHTWINDOW, right_resized_video_frame)
            if cv.waitKey(25) & 0xFF == ord('q'):
                break
            for left_path in left_paths:
                left_video_file_path = left_path \
                    + '/' + os.walk(left_path).__next__()[2][0]
                # print(left_video_file_path)
                left_capture = cv.VideoCapture(left_video_file_path)
                while(left_capture.isOpened()):
                    leftIsTrue, leftFrame = left_capture.read()
                    if leftIsTrue is True:
                        cv.namedWindow(LEFTWINDOW)
                        cv.moveWindow(LEFTWINDOW, 0, 0)
                        left_resized_video_frame = rescaleFrame(
                            leftFrame, 0.75, 0.75
                        )
                        cv.imshow(LEFTWINDOW, left_resized_video_frame)
                        if cv.waitKey(25) & 0xFF == ord('q'):
                            break
                    else:
                        break
        else:
            break
    if(right_capture.isOpened() is False):
        print("Error opening video stream or file")
    right_capture.release()
cv.destroyAllWindows()
