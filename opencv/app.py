import cv2
import time

userInput = input("Make Your Choice (y) for video or (n) for image or (q) for quit: ")


while(userInput != 'q'):
    if(userInput == 'y'):
        cap = cv2.VideoCapture(
            '/home/srpallab/workspace/python_learning/opencv/media/bootup/rightStartFreeply.mp4'
        )
        # Check if camera opened successfully
        if (cap.isOpened() is False):
            print("Error opening video stream or file")
        # Read until video is completed
        while (cap.isOpened()):
            # Capture frame-by-frame
            ret, frame = cap.read()
            if ret is True:
                # Display the resulting frame
                cv2.imshow('Frame', frame)
                # Press Q on keyboard to  exit
                if cv2.waitKey(25) & 0xFF == ord('q'):
                    break
            # Break the loop
            else:
                break
        # When everything done, release the video capture object
        cap.release()
        # Closes all the frames
        cv2.destroyAllWindows()
        userInput = input("Make Your Choice (y) for video or (n) for image or (q) for quit: ")
    elif(userInput == 'n'):
        image = cv2.imread("img/logo.png")
        cv2.imshow("image", image)
        cv2.waitKey(20)
        time.sleep(2)
        cv2.destroyAllWindows()
        userInput = input("Make Your Choice (y) for video or (n) for image or (q) for quit: ")
