import cv2 as cv
import numpy as np
import os

path = r'/home/srpallab/workspace/python_learning/opencv/media'

list_subfolders = [f.path for f in os.scandir(path) if f.is_dir()]

print(list_subfolders)

for subf in list_subfolders:
    seg_sub = [f.name for f in os.scandir(subf) if f.is_dir()]
    print(seg_sub)


def rescaleFrame(frame, scale_width=0.75, scale_height=1.335):
    width = int(frame.shape[1] * scale_width)
    height = int(frame.shape[0] * scale_height)
    dimensions = (width, height)
    return cv.resize(frame, dimensions, interpolation=cv.INTER_AREA)


image_path_list = [
    'media/segment1/left10Million/i0.jpg',
    'media/segment1/leftBattleThePower/i0.jpg'
]
img = cv.imread('media/segment1/left10Million/i0.jpg')
# img = cv.imread('media/segment1/leftBattleThePower/i0.jpg')
# cv.namedWindow("Logo")
# cv.moveWindow("Logo", 1, 1)
# cv.imshow("Logo", rescaleFrame(img, 0.5, 0.69))
# cv.waitKey(1000)


video_path_list = [
    'media/segment1/right10Million/v0.mp4',
    'media/segment1/rightBattleThePower/v0.mp4'
]
# capture = cv.VideoCapture('media/segment1/right10Million/v0.mp4')
capture = cv.VideoCapture('media/segment1/rightBattleThePower/v0.mp4')

# for
while (capture.isOpened()):
    isTrue, frame = capture.read()
    if isTrue is True:
        # frame_resizd = rescaleFrame(frame)
        cv.namedWindow('Video')
        cv.moveWindow('Video', 0, 0)
        resized_image = rescaleFrame(img, 0.5, 0.3828)
        resized_video_frame = rescaleFrame(frame, 0.75, 0.75)
        concat_frame = np.concatenate(
            (resized_image, resized_video_frame),
            axis=1
        )
        cv.imshow('Video', concat_frame)
        if cv.waitKey(25) & 0xFF == ord('q'):
            break
    else:
        break

capture.release()
cv.destroyAllWindows()
