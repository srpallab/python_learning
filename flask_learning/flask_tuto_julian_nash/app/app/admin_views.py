from app import app
from flask import render_template

users = {
    "mitsuhiko": {
        "name": "Armin Ronacher",
        "bio": "Creator of the Flask Framework",
        "twitter_handle": "@mitsuhiko",
    },
    "gvanrossum": {
        "name": "Guido Van Rossum",
        "bio": "Creator of the Python Programming Language.",
        "twitter_handle": "@gvanrossum",
    },
    "elonmusk": {
        "name": "Elon Musk",
        "bio": "Technology enterpreneur, investor and engineer",
        "twitter_handle": "@elonmusk",
    },
}


@app.route("/admin/dashboard")
def admin_dashboard():
    return render_template("admin/dashboard.html")


@app.route("/admin/profile/<username>")
def admin_profile(username):
    user = None
    if username in users:
        user = users[username]
    return render_template("admin/profile.html", user=user)
