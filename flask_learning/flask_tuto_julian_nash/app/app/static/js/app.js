console.log('Hola!');
function submitData(){
    nameInput = document.getElementById('name');
    messageTextBox = document.getElementById('message');

    const entry = {
        name: nameInput.value,
        message: messageTextBox.value
    };
    fetch(`${window.origin}/guest/create-entry`, {
        method: 'POST',
        credentials: "include",
        body: JSON.stringify(entry),
        cache: "no-cache",
        headers: new Headers({
            "content-type": "application/json"
        })
    });
    console.log(entry);
}
