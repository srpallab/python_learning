from app import app
from flask import jsonify, make_response, request


@app.post('/api/json')
def json_example():
    if request.is_json:
        request_json = request.get_json()
        response_body = {
            "message": "JSON Received",
            "sender": request_json.get("name")
        }
        return make_response(jsonify(response_body), 200)
    else:
        return make_response(jsonify({"message": "Request Must Be JSON"}), 400)
