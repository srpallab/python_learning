import os
from app import app
from flask import (render_template, request, redirect, jsonify,
                   make_response, send_from_directory, abort,
                   session, url_for, flash)
from datetime import datetime
from werkzeug.utils import secure_filename


@app.template_filter("clean_date")
def clean_date(dt):
    return dt.strftime("%d %b %Y")


@app.get("/")
def index():
    print(app.config["ENV"])
    # String
    name = "Shafiqur Rhaman"
    # Number
    age = 30
    # List
    super_heros = ["Bat Man", "Spider Man", "Thor", "Iron Man"]
    # Dictionary
    marks = {"English": 77, "Bangla": 70, "Mathematics": 99, "Science": 88}
    # Tuples
    colors = ("Red", "Blue")
    # Boolean
    cold = True

    # Function
    def send_double(num):
        return num * 2

    # class

    class GitRepository:
        def __init__(self, name, description, repo_name):
            self.name = name
            self.description = description
            self.repo_name = repo_name

        def pull(self):
            return f"Pulling from {self.name}"

    python_repo = GitRepository(
        "Python Learing",
        "Learing meterilas only",
        "https://github.com/srpallab/python_repo",
    )

    # date
    date = datetime.utcnow()

    html_text = "<p> This is an IMAGE </p>"

    return render_template(
        "public/index.html",
        name=name,
        age=age,
        super_heros=super_heros,
        marks=marks,
        colors=colors,
        cold=cold,
        send_double=send_double,
        python_repo=python_repo,
        html_text=html_text,
        date=date,
    )


@app.get("/about")
def about():
    return render_template("public/about.html")


@app.get("/contact")
def contact():
    return render_template("public/contact.html")

# import secrets
# secrets.token_urlsafe(16)
# session=ksdfosdfnsdnundsvnSOIDFNSDNVJSND;
# import base64
# base64.b64decode("session=ksdfosdfnsdnundsvnSOIDFNSDNVJSND")


@app.route("/signup", methods=["GET", "POST"])
def signup():
    if request.method == "POST":
        req_data = request.form
        full_name = req_data["full-name"]
        email = req_data.get("email")
        password = req_data["password"]
        if len(password) <= 8:
            flash("Password Must Be 8 Charecter Long.", "Warning")
            return redirect(request.url)
        print(full_name, email, password)

        return redirect(request.url)

    return render_template("public/signup.html")


@app.route("/sign-in", methods=["GET", "POST"])
def signin():
    if request.method == "POST":
        req_data = request.form
        email = req_data["email"]
        password = req_data["password"]
        session["EMAIL"] = email
        print(password)
        return redirect(url_for('profile'))
    return render_template("public/signin.html")


@app.get("/profile")
def profile():
    if session.get("EMAIL", None) is not None:
        return render_template('public/profile')

    return redirect(url_for('signin'))


@app.get('/signout')
def signout():
    session.pop("EMAIL", None)
    return redirect(url_for('signin'))


@app.get('/guests')
def guests_list():
    return render_template("public/guests.html")


@app.post('/guest/create-entry')
def guest_create_entry():
    if request.is_json:
        print(request.get_json())
        return make_response(jsonify({"message": "JSON Succed!"}, 200))
    return make_response(jsonify({"message": "JSON Failed!"}, 400))


@app.get('/query')
def query_string():
    print(request.args)
    print(request.query_string)
    return "Query Received", 200


def allowed_images(filename):
    if "." not in filename:
        return False
    ext = filename.rsplit(".", 1)[1]
    print(ext.upper())
    if ext.upper() in app.config["ALLOWED_IMAGE_EXTENSIONS"]:
        return True
    else:
        return False


def allowed_image_filesize(filesize):
    if int(filesize) <= app.config["MAX_FILESIZE"]:
        return True
    return False


@app.route('/upload', methods=['GET', 'POST'])
def upload_file():
    if request.method == "POST":
        # print(request.files)
        if request.files:
            image = request.files["image"]
            if not allowed_image_filesize(request.cookies["filesize"]):
                print("File is too large")
                return redirect(request.url)
            # print(image)
            # print(os.path.join(app.config['IMAGE_UPLOADS']))
            if image.filename == "":
                print("Image Must have a File name")
                return redirect(request.url)

            if not allowed_images(image.filename):
                print("Image Extention is not allowed")
                return redirect(request.url)
            else:
                filename = secure_filename(image.filename)
            image.save(
                os.path.join(app.config['IMAGE_UPLOADS'], filename)
            )
    return render_template('public/upload_file.html')


"""
string:
int:
float:
path:
uuid:
"""


@app.route('/get-image/<string:image_name>')
def send_images(image_name):
    try:
        return send_from_directory(
            app.config['CLIENT_IMAGE'],
            image_name,
            as_attachment=False
        )
    except FileNotFoundError:
        abort(404)


@app.route('/get-pdf/<string:filename>')
def send_csv(filename):
    try:
        return send_from_directory(
            app.config['CLIENT_PDF'],
            filename,
            as_attachment=False
        )
    except FileNotFoundError:
        abort(404)


@app.route('/get-csv/<path:path>')
def send_files(path):
    try:
        return send_from_directory(
            app.config['CLIENT_CSV'],
            path,
            as_attachment=False
        )
    except FileNotFoundError:
        abort(404)


@app.get('/cookies')
def cookies():
    res = make_response("Cookies", 200)
    cookies = request.cookies
    flavor = cookies.get("flavor")
    print(cookies, flavor)
    res.set_cookie(
        "flavor",
        value="chocolate-chip",
        path=request.path,
        max_age=10,
    )
    res.set_cookie(
        "chocolate type",
        value="dark",
        path=request.path
    )
    res.set_cookie(
        "chewy",
        value="yes",
        path=request.path
    )
    return res
