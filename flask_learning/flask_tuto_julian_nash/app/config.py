class Config(object):
    DEBUG = False
    TESTING = False

    SECRET_KEY = 'this is a secret!'
    DB_NAME = "production-db"
    DB_USERNAME = "root"
    DB_PASSWORD = "example"

    IMAGE_UPLOADS = "/home/srpallab/workspace/python_learning/flask_learning/\
               flask_tuto_julian_nash/app/app/static/img/"
    SESSION_COOKIE_SECURE = True


class DevelopmentConfig(Config):
    DEBUG = True

    SECRET_KEY = 'this is a secret!'
    DB_NAME = "production-db"
    DB_USERNAME = "root"
    DB_PASSWORD = "example"

    IMAGE_UPLOADS = "/home/srpallab/workspace/python_learning/flask_learning/\
flask_tuto_julian_nash/app/app/static/img/uploads"
    ALLOWED_IMAGE_EXTENSIONS = ["PNG", "JPG", "JPEG", "GIF"]
    MAX_FILESIZE = 0.5 * 1024 * 1024
    SESSION_COOKIE_SECURE = True
    CLIENT_IMAGE = '/home/srpallab/workspace/python_learning/flask_learning/\
flask_tuto_julian_nash/app/app/static/client/img'
    CLIENT_CSV = '/home/srpallab/workspace/python_learning/flask_learning/\
flask_tuto_julian_nash/app/app/static/client'
    CLIENT_PDF = '/home/srpallab/workspace/python_learning/flask_learning/\
flask_tuto_julian_nash/app/app/static/client/pdf'


class ProductionConfig(Config):
    pass


class TestingConfig(Config):
    TESTING = True

    SECRET_KEY = 'this is a secret!'
    DB_NAME = "production-db"
    DB_USERNAME = "root"
    DB_PASSWORD = "example"

    IMAGE_UPLOADS = "/home/srpallab/workspace/python_learning/flask_learning/\
               flask_tuto_julian_nash/app/app/static/img/"
    SESSION_COOKIE_SECURE = True
