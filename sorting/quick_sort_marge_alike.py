def qs_s(num):
    less = []
    equal = []
    grater = []
    if len(num) > 1:
        pivot = num[0]
        for i in num:
            if i < pivot:
                less.append(i)
            elif i > pivot:
                grater.append(i)
            else:
                equal.append(i)
        return qs_s(less) + equal + qs_s(grater)
    else:
        return num


if __name__ == "__main__":
    input_numbers = [9, 8, 7, 6, 5, 41, 5, 22, 1]
    len_num = len(input_numbers)
    in_s = qs_s(input_numbers)
    print(in_s)
