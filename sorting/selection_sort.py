def selection_sort(numbers):
    for i in range(len(numbers) - 1):
        i_min = i
        for j in range(i + 1, len(numbers)):
            if numbers[j] < numbers[i_min]:
                i_min = j
            numbers[i], numbers[i_min] = numbers[i_min], numbers[i]


if __name__ == "__main__":
    # input_n = int(input())
    # input_numbers = [int(x) for x in input().split()]
    input_numbers = [4, 5, 6, 3, 2, 1]
    selection_sort(input_numbers)
    print(input_numbers)
