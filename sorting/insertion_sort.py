def insertionSort(array, len_array):
    for i in range(1, len_array):
        value = array[i]
        hole = i
        while hole > 0 and array[hole - 1] > value:
            array[hole] = array[hole - 1]
            hole -= 1
        array[hole] = value


if __name__ == "__main__":
    # input_n = int(input())
    # input_numbers = [int(x) for x in input().split()]
    input_numbers = [9, 8, 7, 4, 52, 2, 4, 2, 6]
    insertionSort(input_numbers, len(input_numbers))
    print(input_numbers)
