"""
This one is in place method.(C style)
This one is much slower for recursion limit in python.
There is much faster one but works like marge sort.
"""


def partitioning(array, start, end):
    pivote = array[end]
    pIndex = start
    for i in range(start, end):
        if array[i] < pivote:
            array[i], array[pIndex] = array[pIndex], array[i]
            pIndex += 1
    array[pIndex], array[end] = array[end], array[pIndex]
    return pIndex


def quick_sort(array, start, end):
    if start < end:
        pIndex = partitioning(array, start, end)
        quick_sort(array, start, pIndex - 1)
        quick_sort(array, pIndex + 1, end)


if __name__ == "__main__":
    # input_n = int(input())
    # input_numbers = [int(x) for x in input().split()]
    input_numbers = [9, 8, 7, 4, 52, 2, 4, 2, 6]
    quick_sort(input_numbers, 0, len(input_numbers) - 1)
    print(input_numbers)
