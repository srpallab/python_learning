def bubble_sort(numbers):
    numbers_len = len(numbers)
    # print(numbers_len)
    for i in range(1, numbers_len):
        flag = 0
        for j in range(0, numbers_len - i):
            # print(f"I = {i}, J = {j}")
            if numbers[j] > numbers[j + 1]:
                numbers[j], numbers[j + 1] = numbers[j + 1], numbers[j]
                flag = 1
        if flag == 0:
            break


if __name__ == "__main__":
    # input_n = int(input())
    # input_numbers = [int(x) for x in input().split()]
    input_numbers = [4, 5, 6, 3, 2, 1]
    bubble_sort(input_numbers)
    print(input_numbers)
