def marge(left, right, array):
    i, j, k = (0, 0, 0)
    left_len, right_len = len(left), len(right)
    while i < left_len and j < right_len:
        if left[i] <= right[j]:
            array[k] = left[i]
            i += 1
        else:
            array[k] = right[j]
            j += 1
        k += 1
    while i < left_len:
        array[k] = left[i]
        i += 1
        k += 1
    while j < left_len:
        array[k] = right[j]
        i += 1
        k += 1


def marge_sort(array):
    len_array = len(array)
    if len_array < 2:
        return array
    mid = len_array // 2
    left = [array[i] for i in range(mid)]
    right = [array[i] for i in range(mid, len_array)]
    marge_sort(left)
    marge_sort(right)
    marge(left, right, array)


if __name__ == "__main__":
    # input_n = int(input())
    input_numbers = [9, 8, 7, 6, 5, 4, 3, 2, 1]
    marge_sort(input_numbers)
    print(input_numbers)
