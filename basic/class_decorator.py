import random


class Elephant:
    def __init__(self, func):
        self._func = func
        self._memory = []

    def __call__(self):
        retvalue = self._func()
        self._memory.append(retvalue)
        return retvalue

    def memory(self):
        return self._memory


app = Elephant


@app
def random_number():
    """
    It returns random odd number
    """
    return random.choice([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15])


print(random_number())
print(random_number.memory())
print(random_number())
print(random_number.memory())
