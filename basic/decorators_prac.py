from functools import wraps


def mapper(func):
    """
    This maps a function on a list
    """
    @wraps(func)
    def inner(list_of_values):
        return [func(value) for value in list_of_values]

    return inner


@mapper
def camelcaser(value):
    """
    This takse a string_like_this then repalce stringLikeThis
    """
    return ''.join([s.capitalize() for s in value.split('_')])


names = [
    'shafiqur_rahman_pallab',
    'sn**p_dog',
    'mapper_in_zapper'
]

print(camelcaser(names))
