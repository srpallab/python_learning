import random


def power_of(arg):
    """
    This return power_of any number given by user
    """
    def decorator(func):

        def inner():
            return func() ** exponent

        return inner

    if callable(arg):
        exponent = 2
        return decorator(arg)
    else:
        exponent = arg
        return decorator


@power_of
def random_odd_number():
    """
    It returns random odd number
    """
    return random.choice([1, 3, 5, 7, 9, 11, 13, 15, 17, 19])


@power_of(0.5)
def random_even_number():
    """
    It returns random odd number
    """
    return random.choice([2, 4, 6, 8, 10, 12, 14, 16, 18])


@power_of(3)
def random_number():
    """
    It returns random odd number
    """
    return random.choice([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15])


print(random_odd_number())
print(random_even_number())
print(random_number())
