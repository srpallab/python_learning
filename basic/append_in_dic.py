if __name__ == '__main__':
    records = {}
    print(type(records))
    for _ in range(int(input())):
        name = input()
        score = float(input())

        if score in records:
            records[score].append(name)
        else:
            records[score] = [name]
    print(records)
