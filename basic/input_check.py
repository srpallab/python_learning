if __name__ == '__main__':
    N = int(input())
    result = []
    for _ in range(N):
        command, *values = input().split(' ')
        # print(command, values)
        values = map(int, values)
        if command == 'insert':
            result.insert(values[0], values[1])
        elif command == 'print':
            print(result)
        elif command == 'remove':
            result.remove(values[0])
        elif command == 'append':
            result.append(values[0])
        elif command == 'sort':
            result.sort()
        elif command == 'pop':
            result.pop()
        elif command == 'reverse':
            result.reverse()
