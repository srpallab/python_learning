if __name__ == '__main__':
    records = []
    for _ in range(int(input())):
        name = input()
        score = float(input())
        if len(records) == 0:
            records.append([name, score])
        else:
            for i in range(len(records)):
                if records[i][1] < score:
                    for j in range(len(records)):
                        if records[j][1] > score:
                            records.insert(j, [name, score])
                            break
                        elif len(records) == (j+1):
                            records.append([name, score])
                            break
                    break
                elif records[i][1] > score:
                    records.insert(i, [name, score])
                    break
                else:
                    records.insert(i + 1, [name, score])
                    break
            print(records)

    names = []
    sec_low = -1
    for g in range(len(records)):
        if records[g][1] < records[g+1][1]:
            sec_low = records[g+1][1]
            break

    for k in range(len(records)):
        if records[k][1] == sec_low:
            names.append(records[k][0])

    names.sort()
    for name in names:
        print(name)
